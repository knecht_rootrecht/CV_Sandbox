#include "CV_Sandbox.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CV_Sandbox w;
    w.show();

    return a.exec();
}
