#ifndef CV_SANDBOX_H
#define CV_SANDBOX_H

#include <QMainWindow>

namespace Ui {
class CV_Sandbox;
}

class CV_Sandbox : public QMainWindow
{
    Q_OBJECT

public:
    explicit CV_Sandbox(QWidget *parent = 0);
    ~CV_Sandbox();

private:
    Ui::CV_Sandbox *ui;
};

#endif // CV_SANDBOX_H
