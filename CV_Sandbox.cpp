#include "CV_Sandbox.h"
#include "ui_CV_Sandbox.h"

CV_Sandbox::CV_Sandbox(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CV_Sandbox)
{
    ui->setupUi(this);
}

CV_Sandbox::~CV_Sandbox()
{
    delete ui;
}
